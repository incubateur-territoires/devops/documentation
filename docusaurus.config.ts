import {themes as prismThemes} from 'prism-react-renderer';
import type {Config} from '@docusaurus/types';
import type * as Preset from '@docusaurus/preset-classic';

const config: Config = {
  title: 'DevOps doc',
  tagline: 'La base de documentation Ops de l\'incubateur des territoires de l\'ANCT',

  url: 'https://devops.incubateur.anct.gouv.fr/',
  baseUrl: '/',

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  markdown: {
    mermaid: true,
  },
  presets: [
    [
      'classic',
      {
        docs: {
          path: 'content',
          routeBasePath: 'content',
          sidebarPath: './sidebars.ts',
        },
        theme: {
          customCss: [
            require.resolve('dsfr-connect/dist/infima-v1/index.css'),
            require.resolve('./src/css/main.scss'),
          ],
        },
      } satisfies Preset.Options,
    ],
  ],
  plugins: [
    'docusaurus-plugin-sass',
  ],
  themeConfig: {
    tableOfContents: {
      minHeadingLevel: 2,
      maxHeadingLevel: 6,
    },
    sidebar: {
      hideable: true,
      autoCollapseCategories: true,
    },
    navbar: {
      title: 'Documentation',
      logo: {
        alt: '',
        src: 'img/logo.svg',
      },
      items: [
        {to: '/content/doc', label: 'Documentation', position: 'left'},
        {to: '/content/cookbook', label: 'Cookbook', position: 'left'},
        {to: '/content/good_practices', label: 'Bonnes pratiques', position: 'left'},
        {to: '/content/glossaire', label: 'Glossaire', position: 'left'},
        {to: '/content/faqs', label: 'FAQ', position: 'left'},
      ],
    },
    footer: {
      logo: {
        src: 'img/logo.svg',
      },
      links: [
        {
          label: 'Gitlab',
          href: 'https://gitlab.com/incubateur-territoires/devops/documentation',
        },
        ],
      copyright: `Sauf mention contraire, tous les contenus de ce site sont sous <a href="https://raw.githubusercontent.com/inclusion-numerique/mediature/main/LICENSE">licence AGPL-3.0</a>`,
    },
    prism: {
      theme: prismThemes.github,
      darkTheme: prismThemes.dracula,
    },
  } satisfies Preset.ThemeConfig,
  themes: [
    [
      require.resolve('@easyops-cn/docusaurus-search-local'),
      // /** @type {import("@easyops-cn/docusaurus-search-local").PluginOptions} */
      {
        hashed: true,
        language: ['fr'],
      },
      ],
    ],
};

export default config;
