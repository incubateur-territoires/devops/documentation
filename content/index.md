# Introduction à l'infrastructure de l'incubateur

Bienvenue sur la documentation de l'infrastructure de notre incubateur. Vous trouverez ici des informations essentielles qui vous aideront à comprendre l'environnement technique dans lequel vous allez évoluer, et comment tirer le meilleur parti des outils à votre disposition.

## Qu'est-ce que l'infrastructure de notre incubateur ?

L'infrastructure de notre incubateur est conçue pour être moderne, évolutive et sécurisée. Actuellement, nous nous appuyons entièrement sur Kubernetes, un système d'orchestration open-source qui automatise le déploiement, la montée en charge et la gestion des applications conteneurisées. Cela permet non seulement une grande flexibilité et une facilité de déploiement mais aussi une meilleure gestion des ressources et une scalabilité efficace.

Pour nos besoins en Infrastructure as a Service (IaaS), nous sommes partenaires avec Scaleway, un prestataire reconnu qui nous offre un large éventail de services cloud. L'utilisation de Scaleway comme base pour notre PaaS nous assure une bonne intégration avec Kubernetes tout en bénéficiant d'un écosystème et d'un support professionnels.

Dans cette optique d'agilité et de contrôle fin sur notre infrastructure, nous employons également Terraform, un outil d'Infrastructure as Code (IaC) qui permet de créer, modifier et versionner de manière sécurisée et efficace notre infrastructure. Terraform introduit une approche déclarative pour définir l'infrastructure, ce qui signifie que vous spécifiez une configuration souhaitée et Terraform s'occupe de la réaliser. Cela est fait sans intervention manuelle, en assurant une cohérence entre les environnements de développement, de test et de production. En utilisant Terraform au sein de notre incubateur, nous rationalisons le déploiement des ressources et des configurations nécessaires à nos projets, tout en garantissant une reproductibilité.

Par ailleurs, certains projets au sein de l'incubateur peuvent avoir des besoins spécifiques ne nécessitant pas toutes les capacités offertes par Kubernetes ou recherchant une approche différente dans la gestion de leur infrastructure. Pour ces cas-là, des solutions PaaS telles que celles proposées par Scaleway sont utilisées. Ces solutions clés en main permettent la mise en place rapide de conteneurs, bases de données ou encore d'environnements de stockage adaptés aux exigences particulières de ces projets.

## Comment obtenir un accès Kubernetes ?

Pour demander un accès à notre cluster Kubernetes, il vous suffit de contacter l'équipe dédiée à la gestion de l'infrastructure via mattermost. Cette approche permet d'assurer que tous les membres disposent des droits nécessaires pour travailler efficacement tout en maintenant la sécurité des environnements.

Pour chaque personne, nous générons des fichiers `kubeconfig` par environnement mis à disposition.

## Outils à disposition sur Kubernetes

Une fois intégré(e) au cluster Kubernetes, différents outils sont mis à votre disposition pour faciliter le développement et le déploiement de vos applications :

1. **Surveillance et monitoring :** Une stack de monitoring existe, utilisant Prometheus, Loki et Grafana. Demandez des accès aux projets grafana de vos applications.
2. **CI/CD avec GitLab :** Le déploiement continu est facilité grâce à des pipelines CI/CD configurables depuis GitLab.
3. **Stockage persistant :** Pour assurer la persistance de vos données au-delà du cycle de vie d'un pod, Kubernetes permet l'utilisation de volumes persistants, qui utilisent les Block Storages de l'IaaS.
4. **Services réseau :** Configuration simplifiée du réacheminement du trafic avec haproxy en tant qu'ingress controller.

## Déploiement via Gitlab

Le travail collaboratif est encouragé grâce à la mise en place d'une intégration avec GitLab. Il sert non seulement comme dépôt pour votre code source mais aussi comme outil pour configurer vos pipelines CI/CD afin d’automatiser votre processus de déploiement.

Votre pipeline peut être personnalisé selon les besoins spécifiques de vos projets. De plus, notre équipe DevOps est disponible pour accompagner la mise en place d'une pipeline CI/CD optimisée et adaptée aux meilleures pratiques actuelles.

## Gestion des noms de domaine et cert-manager

Pour la gestion des noms de domaine ainsi que le provisionnement automatisé des certificats TLS/SSL, nous utilisons `cert-manager`. C'est un outil intégré à Kubernetes qui permet de simplifier la création, le renouvellement et l'utilisation des certificats nécessaires pour sécuriser les communications vers vos services.
 