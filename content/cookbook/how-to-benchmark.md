# Comment faire un benchmark ?

Dans le cadre de l'évaluation de performances des applications web, choisir les bonnes métriques est essentiel pour garantir que le benchmark reflète fidèlement la qualité et l'efficacité des différentes facettes de l'application. L'utilisation de métriques adéquates nous permet d'avoir une compréhension précise des points forts et des axes d'amélioration, ce qui a un impact direct sur l'expérience utilisateur·rice.

Voici quelques-unes des métriques importantes à considérer pour un benchmark d'applications web :

1. **Temps de chargement (Load Time):**  
Il s'agit du temps nécessaire pour charger complètement une page web. C'est une métrique cruciale, car un temps de chargement long peut entraîner une mauvaise expérience utilisateur·rice et augmenter le taux de rebond.

2. **Temps jusqu'à interactif (Time to Interactive, TTI):**  
TTI mesure le temps que cela prend pour qu'une page devienne complètement interactive. Cela signifie que les éléments visibles sont affichés et que l'utilisateur·rice peut interagir avec eux sans délai notable.

3. **Taux d'erreur:**  
Le taux d'erreur indique la fréquence à laquelle les utilisateur·rice·s rencontrent des erreurs lorsqu'ils ou elles utilisent l'application. Des erreurs fréquentes peuvent indiquer un problème sous-jacent de stabilité ou de fiabilité.

4. **Nombre de requêtes par seconde (Requests Per Second, RPS):**  
Cette métrique fait référence au nombre de requêtes que le serveur peut traiter chaque seconde. Elle est importante pour évaluer la capacité de l'application à gérer le trafic des utilisateur·rice·s, en particulier lors des pics d'utilisation.

5. **Latence:**  
La latence représente le retard entre un appel client et une réponse du serveur. Une faible latence est essentielle pour une application réactive et rapide.

6. **Débit (Throughput):**  
Le débit mesure la quantité totale de travail effectué par l'application dans un temps donné, tel que le nombre de transactions traitées ou la quantité de données transmises.

7. **Utilisation des ressources:**  
Il est important de surveiller comment l'application utilise les ressources système, comme la CPU, la mémoire RAM et l'espace disque. Une utilisation excessive peut indiquer un manque d'efficacité ou un besoin d'optimisation.

8. **Scalabilité:**  
Les tests de scalabilité nous indiquent comment l'application se comporte sous différentes charges utilisateur·rice·s et si elle peut se développer pour gérer plus d'utilisateur·rice·s sans baisse significative des performances.

## Comment procéder à un benchmark :

1. **Choisir les outils :** Utiliser des outils appropriés qui peuvent mesurer avec précision les métriques mentionnées ci-dessus.

2. **Établir un environnement contrôlé :** Réaliser les benchmarks dans un environnement représentatif mais stable afin que les résultats soient reproductibles.

3. **Créer des scénarios d’utilisation réels :** Simuler des conditions qui correspondent à ceux rencontrés par vos utilisateur·rice·s typiques.

4. **Examiner et analyser les données :** Les données brutes doivent être examinées avec soin pour identifier tout problème potentiel et mettre en place des améliorations fondées sur cela.

## Quels outils pour un benchmark web ?

1. **k6**:
   - **Usage**: k6 est un outil de test de performance moderne conçu pour tester la charge et les performances des applications et des services web.
   - **Caractéristiques**: Il permet de simuler du trafic provenant de différents endroits et de tester divers scénarios d'utilisation pour voir comment votre API se comporte sous stress. Les scripts de test k6 sont écrits en JavaScript, offrant une grande flexibilité pour définir des cas d'utilisation personnalisés.

2. **Sitespeed.io**:
   - **Usage**: Sitespeed.io est un ensemble d'outils open source pour l'analyse de performance des sites web.
   - **Caractéristiques**: Il peut être utilisé pour surveiller la performance des Web APIs, en se concentrant sur des aspects tels que le temps de réponse et la vitesse de chargement. Sitespeed.io est utile pour obtenir un aperçu détaillé des performances de votre API en termes de temps de chargement et d'optimisation.

### Benchmark serveur : k6

#### Métriques spécifiques

**Temps de Réponse (Response Time)** :
   - **Description**: Le temps que met une requête pour recevoir une réponse complète de l'API.
   - **Interprétation**: Des temps de réponse plus courts indiquent une meilleure performance. Des temps de réponse longs peuvent indiquer un goulot d'étranglement dans le traitement des requêtes ou des problèmes de réseau.

**Débit (Throughput)** :
   - **Description**: Le nombre de requêtes traitées par unité de temps (par exemple, requêtes par seconde).
   - **Interprétation**: Un débit élevé signifie que l'API peut gérer un grand nombre de requêtes simultanément. Un débit faible peut indiquer des limitations de performance.

**Taux d'Erreur (Error Rate)** :
   - **Description**: Le pourcentage de toutes les requêtes qui ont abouti à une erreur.
   - **Interprétation**: Un taux d'erreur élevé peut indiquer des problèmes avec l'API, tels que des erreurs de serveur ou des défaillances de traitement.

**Temps de Connexion (Connection Time)** :
   - **Description**: Le temps nécessaire pour établir une connexion entre le client et le serveur.
   - **Interprétation**: Des temps de connexion longs peuvent indiquer des problèmes de réseau ou des problèmes avec le serveur web lui-même.

**Concurrence (Concurrency)** :
   - **Description**: Le nombre de requêtes traitées simultanément par l'API.
   - **Interprétation**: La capacité à gérer un grand nombre de requêtes simultanées est essentielle pour la scalabilité. Des problèmes à des niveaux élevés de concurrence peuvent indiquer des problèmes de scalabilité.

**Utilisation des Ressources (Resource Utilization)** :
   - **Description**: L'utilisation des ressources du serveur (CPU, mémoire, disque, bande passante).
   - **Interprétation**: Une utilisation élevée des ressources peut indiquer un besoin de mieux optimiser le code de l'API ou de mettre à l'échelle l'infrastructure.

**Latence (Latency)** :
   - **Description**: Le temps pris pour qu'un paquet de données voyage de la source au destinataire.
   - **Interprétation**: La latence élevée peut affecter négativement l'expérience utilisateur, surtout dans les applications temps réel.

**Percentiles (P50, P95, P99)** :
   - **Description**: Ces percentiles indiquent le temps de réponse sous lequel un certain pourcentage de toutes les requêtes sont complétées.
   - **Interprétation**: Par exemple, P95 à 200 ms signifie que 95% des requêtes sont traitées en moins de 200 ms. Des percentiles élevés dans les valeurs supérieures peuvent indiquer des problèmes de performance sporadiques.

#### Exemples
##### Exemple : Test de Charge Basique

```javascript
import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
    stages: [
        { duration: '30s', target: 20 }, // Rampe jusqu'à 20 utilisateurs en 30 secondes
        { duration: '1m', target: 20 }, // Maintient 20 utilisateurs pendant 1 minute
        { duration: '30s', target: 0 }, // Rampe jusqu'à 0 utilisateurs en 30 secondes
    ],
};

export default function () {
    let response = http.get('http://votreapi.com/endpoint');

    check(response, { 'statut est 200': (r) => r.status === 200 });
    sleep(1);
}
```
Ce script augmente progressivement le nombre d'utilisateurs, maintient une charge constante, puis diminue le nombre d'utilisateurs.

##### Exemple : Test de Scénario d'Utilisateur

```javascript
import http from 'k6/http';
import { check, group, sleep } from 'k6';

export let options = {
    stages: [
        { duration: '1m', target: 50 },
        { duration: '3m', target: 50 },
        { duration: '1m', target: 0 },
    ],
};

function loginUser() {
    var payload = JSON.stringify({ email: 'user@example.com', password: 'supersecret' });
    var params = { headers: { 'Content-Type': 'application/json' } };

    let loginResponse = http.post('http://votreapi.com/auth/login', payload, params);

    check(loginResponse, { 'login réussi': (resp) => resp.json('token') !== '' });
    return loginResponse.json('token');
}

export default function () {
    let authToken = loginUser();

    group('actions utilisateur', function () {
        let headers = { headers: { Authorization: `Bearer ${authToken}` } };
        http.get('http://votreapi.com/user/actions', headers);
        // Autres actions de l'utilisateur
    });

    sleep(1);
}
```
Ce script comprend la connexion de l'utilisateur, l'exécution d'actions avec authentification, et la déconnexion.

##### Exemple : Test de Performance avec des Paramètres Personnalisés

```javascript
import http from 'k6/http';
import { check } from 'k6';

export let options = {
    thresholds: {
        http_req_duration: ['p(95)<500'], // 95% des requêtes doivent être sous 500ms
    },
    vus: 10,
    duration: '30s',
};

export default function () {
    let response = http.get('http://votreapi.com/endpoint');
    check(response, { 'temps de réponse < 500ms': (r) => r.timings.duration < 500 });
}
```
Ce script définit des seuils spécifiques pour les temps de réponse et exécute un nombre constant d'utilisateurs virtuels.

### Benchmark client : sitespeed.io

#### Exemples
##### Exemple : Analyse de Base d'une Page Web ou d'une API
Pour analyser la performance d'une page web ou d'une API, vous pouvez exécuter Sitespeed.io avec l'URL cible. Par exemple, pour analyser une page web :

```bash
sitespeed.io https://votresite.com/chemin
```
##### Exemple : Analyse avec des Options Spécifiques
Vous pouvez personnaliser l'analyse en utilisant diverses options. Par exemple, pour définir un nombre spécifique de runs (itérations) :

```bash
sitespeed.io --browsertime.iterations 5 https://votreapi.com/endpoint
```

##### Exemple : Analyse de Performance sur Mobile
Pour simuler la performance sur un appareil mobile, vous pouvez utiliser l'option `--mobile` :

```bash
sitespeed.io --mobile https://votreapi.com/endpoint
```

##### Exemple : Utilisation de Graphite et Grafana pour le Suivi Continu
Si vous souhaitez surveiller la performance de votre site ou API sur le long terme, vous pouvez configurer Sitespeed.io pour envoyer des données à Graphite, puis visualiser les résultats avec Grafana.

```bash
sitespeed.io --graphite.host=adresse.graphite --graphite.port=port https://votreapi.com/endpoint
```

### Benchmark d'un applicatif spécifique avec hyperfine

Hyperfine est un outil de benchmarking en ligne de commande pour mesurer et comparer les performances des programmes. Bien qu'il soit principalement utilisé pour des programmes exécutables, on peut l'adapter pour tester des requêtes web ou des scripts spécifiques qui interagissent avec une Web API. Voici quelques exemples :

#### Exemples

##### Exemple : Benchmark Basique d'une Commande
Si vous avez un script qui interroge une API, vous pouvez mesurer son temps d'exécution avec Hyperfine. Par exemple, si vous avez un script `query_api.sh` :

```bash
hyperfine './query_api.sh'
```

##### Exemple : Comparaison de Deux Commandes
Vous pouvez comparer les performances de deux scripts ou commandes différents. Par exemple, pour comparer deux scripts différents interrogeant la même API :

```bash
hyperfine './query_api_v1.sh' './query_api_v2.sh'
```

##### Exemple : Utilisation d'Options Avancées

```bash
hyperfine --warmup 3 --min-runs 10 './query_api.sh'
```

Ici, `--warmup 3` exécutera la commande trois fois sans enregistrer les résultats pour "chauffer" le système, et `--min-runs 10` indiquera à Hyperfine d'exécuter la commande au moins dix fois pour obtenir des mesures fiables.

##### Exemple : Benchmark d'une Requête cURL
Si vous souhaitez mesurer le temps de réponse d'une requête API directement, vous pouvez utiliser cURL dans Hyperfine :

```bash
hyperfine 'curl -s https://votreapi.com/endpoint'
```

##### Exemple : Exportation des Résultats
Hyperfine permet d'exporter les résultats sous différents formats pour une analyse plus poussée :

```bash
hyperfine --export-json results.json './query_api.sh'
```

Cela exécutera le test et enregistrera les résultats dans un fichier JSON pour un examen ultérieur.
