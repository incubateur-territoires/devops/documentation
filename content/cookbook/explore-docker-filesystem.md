# Comment visualiser le système de fichiers d'une image docker ?

Pour cela, vous pouvez utiliser `docker cp` (ou `podman cp`, podman est un bon remplaçant pour docker sans nécessité d'avoir un processus de fond) :  Il suffit de créer un container (il n'est pas nécessaire de le lancer), puis copier son contenu.

:::info

1. **Créer un conteneur** : Utiliser `docker create` pour créer un conteneur à partir de l'image Docker, sans le lancer.
2. **Copier les fichiers** : Employer `docker cp` pour copier les fichiers du conteneur vers le système hôte.
3. **Supprimer le conteneur** : Après exploration, utiliser `docker rm` pour supprimer le conteneur.

:::

## Procédure

1. **Créer un conteneur à partir de l'image Docker**. Pour ce faire, utilisez la commande `docker create`. Cette commande créera un conteneur à partir de votre image Docker, mais elle ne le lancera pas. Par exemple, pour une image nommée `monimage`, la commande serait :

    *Note : Si l'image n'est pas présente en local, elle sera téléchargée.*

```bash
docker create --name monconteneur monimage
```

2. **Copier les fichiers du conteneur vers votre système hôte**. Après avoir créé le conteneur, vous pouvez utiliser la commande `docker cp` pour copier des fichiers du conteneur vers votre système hôte. Par exemple, pour copier tout le système de fichiers du conteneur à un emplacement de votre choix, vous pouvez utiliser la commande suivante :

```bash
docker cp monconteneur:/ ./
```

Cela copiera tout le système de fichiers du conteneur (indiqué par `:/`) dans le répertoire courant de votre système hôte (indiqué par `./`).

3. **Supprimer le conteneur une fois que vous avez terminé**. Après avoir exploré le système de fichiers, vous pouvez supprimer le conteneur avec la commande `docker rm`. Par exemple :

```bash
docker rm monconteneur
```

## Exemple

Voici un exemple complet utilisant une image Docker officielle de Ubuntu :

```bash
# Créer un conteneur à partir de l'image Docker d'Ubuntu
docker create --name temp_container ubuntu

# Copier les fichiers du conteneur vers votre système hôte
docker cp temp_container:/ ./filesystem

# Supprimer le conteneur
docker rm temp_container
```

Dans cet exemple, tous les fichiers du système de fichiers Ubuntu sont copiés dans un répertoire nommé `filesystem` dans votre répertoire courant. Vous pouvez ensuite explorer ce répertoire à votre guise, sans avoir à lancer l'image Docker.

**Note :** Vous pouvez également utiliser des outils comme [Dive](https://github.com/wagoodman/dive) pour explorer les images Docker. Dive permet d'explorer les couches d'une image Docker et voir quels fichiers ont été modifiés dans chaque couche.
