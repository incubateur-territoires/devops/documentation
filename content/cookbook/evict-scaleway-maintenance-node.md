# Procédure d'éviction d'un nœud Kubernetes planifié pour maintenance par Scaleway

:::info

1. **Marquage du nœud comme "cordon"** : Empêche de nouveaux pods d'être programmés sur le nœud.
2. **Suppression des pods du nœud** : Les pods existants sur le nœud sont identifiés et supprimés pour se recréer ailleurs.
3. **Vérification de la recréation des pods** : Assure que les pods supprimés sont recréés et s'exécutent sur d'autres nœuds.
4. **Drainage du nœud** : Evacue tous les pods du nœud et le prépare pour la maintenance.
5. **Extinction du nœud via la console Scaleway (si nécessaire)** : En cas de non-suppression automatique du nœud, l'éteindre manuellement.

:::

## Introduction

Lorsque Scaleway planifie une maintenance pour un de nos nœuds Kubernetes, il est essentiel d'assurer que vos workloads continuent de s'exécuter sans interruption. Cette procédure guide l'éviction en toute sécurité d'un nœud marqué pour maintenance.
Le but est de le faire de manière maitrisée plutôt qu'attendre la maintenance.

## Étapes

### 1. Marquer le nœud comme "cordon"

Cette étape empêche que de nouveaux pods soient programmés sur le nœud.

```bash
kubectl cordon <NOM_DU_NOEUD>
```


### 2. Supprimer les pods du nœud

Identifiez et supprimez tous les pods sur le nœud concerné pour qu'ils se récréent ailleurs.
Pour se faire, vous pouvez utiliser `kubectl describe node <NOM_DU_NOEUD>`.
  
Vous pouvez également scripter ainsi : 

```bash
#!/bin/bash

if [ -z "$1" ]; then
  echo "Usage: $0 <NOM_DU_NOEUD>"
  exit 1
fi

NODE_NAME=$1

# Obtenir tous les pods sur le nœud
pods=$(kubectl get pods --all-namespaces -o=jsonpath='{range .items[?(@.spec.nodeName=="'$NODE_NAME'")]}{.metadata.namespace}{" "}{.metadata.name}{"\n"}{end}')

# Supprimer les pods
for pod in $pods; do
  NAMESPACE=$(echo $pod | awk '{print $1}')
  POD_NAME=$(echo $pod | awk '{print $2}')
  
  echo "Suppression du pod $POD_NAME dans le namespace $NAMESPACE"
  kubectl delete pod $POD_NAME -n $NAMESPACE --grace-period=30
done
```

Et l'utiliser ainsi `./script_name.sh <NOM_DU_NOEUD>`

### 2.1. Vérifiez que les pods sont recréés

Assurez-vous que tous les pods que vous avez supprimés sont correctement recréés et s'exécutent sur d'autres nœuds.

```bash
kubectl get pods -o wide | grep <NOM_DU_NOEUD>
```


### 3. Drainer le nœud

Cette étape évacue tous les pods du nœud et le prépare pour la maintenance.

Le `drain` va aller supprimer les pods, mais il est préférable de le faire soi-même en amont (étape 2), pour être capable de mieux comprendre et réagir en cas de problème.

```bash
kubectl drain <NOM_DU_NOEUD> --delete-emptydir-data --ignore-daemonsets
```

L'option `--delete-emptydir-data` s'assure que les pods utilisant des volumes `emptyDir` sont correctement évacués, et l'option `--ignore-daemonsets` permet d'ignorer les pods qui sont gérés par des DaemonSets.

### 4. Éteindre le nœud via la console Scaleway (si nécessaire)

Si le nœud n'est pas supprimé automatiquement lors d'un scale-down, vous pouvez l'éteindre :

1. Connectez-vous à votre compte Scaleway.
2. Naviguez vers la section des instances.
3. Trouvez et sélectionnez le nœud concerné.
4. Utilisez l'option pour éteindre l'instance (power off).
