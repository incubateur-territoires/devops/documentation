# Explorer mon environnement

:::info

1. **Configuration de Kubectl avec Kubeconfig** : Définir la variable d'environnement `KUBECONFIG` pour utiliser `kubectl` avec le bon cluster Kubernetes.
2. **Utilisation de Helm pour lister les applications déployées** : Configurer Helm avec le fichier kubeconfig, puis exécuter `helm list` pour voir les déploiements dans Kubernetes.
3. **Exploration des Ressources avec Kubectl** : Utiliser `kubectl get pods --all-namespaces` pour voir les pods, et `kubectl describe pod <nom_du_pod>` pour plus de détails sur un pod spécifique.

:::

## Objectif
L'objectif de cette documentation est de vous fournir les étapes et justifications nécessaires pour surveiller et comprendre quels services et applications s'exécutent dans vos différents environnements Kubernetes, tels que la production, le développement, et les applications de revue. Nous expliquerons comment utiliser votre configuration `kubeconfig` associée à chaque environnement ainsi que l'outil de gestion de paquets `Helm` pour obtenir un aperçu détaillé des déploiements.

## Pourquoi surveiller les ressources dans nos environnements ?

Surveiller ce qui est déployé dans nos environnements nous permet de :

- **Assurer la conformité** : s'assurer que seules les applications autorisées sont déployées.
- **Faciliter le dépannage** : identifier rapidement quels services peuvent être en cause en cas d'incident.
- **Optimiser les ressources** : savoir ce qui est exécuté pour ajuster les allocations et supprimer les services inutiles.

## Prérequis

- Un fichier `kubeconfig` valide par environnement.
- L'outil `kubectl` installé sur votre machine.
- L'outil `helm` installé sur votre machine.

## Étape 1 : Configuration de Kubectl avec Kubeconfig

Kubectl est l'outil en ligne de commande pour interagir avec le cluster Kubernetes. Chaque fichier kubeconfig contient les informations d'accès à un cluster spécifique.

### Comment le configurer ?

1. **Positionner la variable d'environnement** `KUBECONFIG` pour pointer vers votre fichier kubeconfig. Cela permet à `kubectl` de trouver automatiquement les informations nécessaires pour se connecter à l'environnement voulu.

```sh
export KUBECONFIG=/chemin/vers/votre/fichier-kubeconfig
```

2. **Valider la connexion au cluster** en exécutant une commande simple comme `kubectl get pods`. Vous devriez voir la liste des pods de l'environnement correspondant au fichier kubeconfig spécifié.

### Bonnes pratiques

- **Sécuriser vos fichiers kubeconfig**, ne partagez jamais vos fichiers kubeconfig car ils donnent accès à vos clusters.
- Limitez les droits d'accès au fichier kubeconfig (`chmod` \<= 600`)

## Étape 2 : Utilisation de Helm pour lister les applications déployées

`Helm` est un gestionnaire de paquets pour Kubernetes, permettant de simplifier le déploiement et la gestion d'applications.

### Pourquoi utiliser Helm ?

Helm présente plusieurs avantages :

- Il permet une **gestion versionnée et reproductible** des déploiements.
- Il supporte des fichiers de configuration personnalisés pour adapter le déploiement à l'environnement.
- Il propose un mécanisme de publication appelé charts qui encapsule tous les fichiers nécessaires au déploiement d'une application.

### Comment lister les déploiements avec Helm ?

1. Assurez-vous que Helm est correctement configuré avec l'environnement ciblé grâce au fichier kubeconfig précédemment configuré.

2. **Exécutez la commande Helm suivante** :

   ```sh
   helm list
   ```

   Cette commande affichera toutes les installations Helm, y compris celles qui sont suspendues ou échouées, dans tous les namespaces du cluster.

### Bonnes pratiques

- Nommez vos releases Helm de manière cohérente pour faciliter l'identification du service qu'elles représentent.

## Étape 3 : Exploration des Ressources avec Kubectl

Pour obtenir plus de détails sur ce qui s'exécute réellement sur votre cluster, vous pouvez utiliser kubectl pour inspecter différentes ressources telles que pods, services ou deployments.

### Comment inspecter vos ressources ?

1. Obtenez la liste des pods en cours d'exécution avec :

   ```sh
   kubectl get pods --all-namespaces
   ```

2. Si vous souhaitez voir les détails d'un pod spécifique ou autre ressource Kubernetes (comme `deployments`, `services`, etc.), utilisez le nom du pod avec :

   ```sh
   kubectl describe pod <nom_du_pod>
   ```
