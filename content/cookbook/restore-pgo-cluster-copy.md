# Procédure pour restaurer un cluster PostgreSQL PGO sur une instance à part

PGO permet de restaurer un cluster PostgreSQL depuis les sauvegardes. Cette restauration peut se faire sur le même cluster, ou sur un cluster à part dans le cas où on aurait seulement besoin d'accèder à d'anciennes données tout en gardant l'état courant de l'application.

:::info

1. **Récupérer la configuration actuelle** du cluster à restaurer.
2. **Modifier le nom du cluster** dans la configuration pour éviter les conflits.
3. **Retirer les éléments de configuration non nécessaires** pour le réplica.
4. **Configurer la restauration** à partir des sauvegardes d'un cluster existant.
5. **Configurer un utilisateur** pour accéder aux données restaurées.
6. **Appliquer la configuration** pour créer le nouveau cluster.

:::

## Étapes

### 1. Récupérer la définition actuelle du cluster dont on veut restaurer une sauvegarde

Il est préférable de créer la copie sur un cluster qui utilise les mêmes caractéristiques que le cluster origninal.
Ce sont notamment les définitions des images de conteneur et la version de PostgreSQL.
Si le namespace impose des quotas de ressources, il faudra aussi spécifier ces ressources pour différents conteneurs.

Le plus simple pour récupérer une configuration complète est d'utiliser celle du cluster existant.

```bash
CLUSTER_NAME=my-cluster
kubectl get postgrescluster $CLUSTER_NAME -o yaml > current_cluster.yaml
cp current_cluster.yaml replica_cluster.yaml
```

La configuration sera éditée sur le fichier `replica_cluster.yaml`. Garder une copie de la configuration originale peut être utile pour revoir la différence entre les configurations au besoin.

### 2. Changer le nom du cluster

Le champ `.metadata.name` peut être changé pour donner un nouveau nom au cluster.
C'est important pour être sûr de ne pas reconfigurer le cluster existant.

### 3. Retirer les éléments de configuration non souhaités

Le réplica n'a pas forcément besoin de toutes les fonctionnalités du cluster original.
Notamment, ce qui peut être retiré:
- Les backups sur S3 et le secret configuration les backups sur S3. Le backup sur un repo local doit quand même être configuré pour que la configuration PGO soit valide.
- Les annotations et labels comme toutes celles qui portent le texte `Helm` ou `helm`.
- Les données spécifiques au cluster déployé, comme `.metadata.creationTimestamp`, `.metadata.generation`, `.metadata.resourceVersion`, `.metadata.uid`
- Toute la partie `.status`

Il est aussi possible de réduire le nombre de réplicas si la haute disponibilité n'est pas souhaitée pour la nouvelle instance, ou changer les limites de ressources pour correspondre à une plus faible utilisation du nouveau cluster.

### 4. Spécifier la restauration depuis un cluster existant

Il faut maintenant configurer la restauration depuis les sauvegardes existances.
Ceci se fait avec la configuration suivante :
```yaml
spec:
  dataSource:
    postgresCluster:
      clusterName: my-cluster
      repoName: repo1
      options:
      - --type=time
      - --target="2023-11-15 23:59:59+01"
```
Ici, nous configurons le nouveau cluster pour qu'il utilise les sauvegardes du cluster `my-cluster` pour le restaurer à la date du 15 novembre 2023 à 23h59 et 59 secondes dans le fuseau horaire UTC+01.
Les sauvegardes étant faites avec pgbackrest qui sauvegarde le WAL, il est possible de faire des restaurations à n'importe quelle date entre deux sauvegardes.

### 5. Configurer un utilisateur pour accéder aux données restaurées

PGO va restaurer les données dans une base de données avec le nom d'origine et le propriétaire d'origine, et créer par défaut un accès avec un nouveau nom d'utilisateur identique au nom du réplica qui n'aura pas accès à la base de données d'origine.
Il est utile alors de spécifier un utilisateur à créer avec le même nom que l'utilisateur d'origine.

```yaml
spec:
  users:
    - name: my-cluster
      databases: my-cluster
```

### 6. Appliquer la configuration pour créer le cluster

Il suffit d'appliquer la configuration avec `kubect apply -f replica_cluster.yaml`.
Un pod apparaîtra pour restaurer le nouveau cluster depuis la sauvegarde, puis l'instance démarrera et sera accessible.
Les informations de connexion seront disponibles dans le secret `<nom du nouveau cluster>-pguser-my-cluster`.
