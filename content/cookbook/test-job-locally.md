# Tester un job en local

Exécuter un job de CI/CD en local à des fins de test peut être très utile pour plusieurs raisons :

1. **Débogage rapide** : Si un job échoue dans le pipeline de CI/CD, il peut être plus rapide et plus facile de reproduire le problème localement pour le débogage.

2. **Développement itératif** : Lors de la création ou de la modification d'un job de CI/CD, il est souvent plus efficace de tester le job localement avant de le pousser vers le dépôt. Cela permet d'éviter d'avoir à attendre que le pipeline entier s'exécute pour chaque petit changement.


En ce qui concerne le fonctionnement de l'exécution d'un job en local à l'aide de `gitlab-runner`, voici un aperçu général :

1. **Installation de `gitlab-runner` et de Docker** : Vous aurez besoin de `gitlab-runner` et de Docker installés sur votre machine. `gitlab-runner` est l'outil qui interagit avec GitLab pour exécuter les jobs, et Docker est nécessaire car `gitlab-runner` exécute les jobs dans des conteneurs Docker.

2. **Configuration du job** : Les jobs sont définis dans le fichier `.gitlab-ci.yml` de votre projet. Chaque job a un nom et spécifie une série de commandes à exécuter. Vous pouvez également spécifier une image Docker à utiliser pour le job, ce qui détermine l'environnement dans lequel les commandes sont exécutées.

3. **Exécution du job** : Pour exécuter un job spécifique localement, vous utilisez la commande `gitlab-runner exec docker my-job-name`. `gitlab-runner` crée un nouveau conteneur Docker en utilisant l'image spécifiée dans votre fichier `.gitlab-ci.yml`, puis exécute les commandes du job dans ce conteneur.

## Configuration

Lorsque vous enregistrez un runner, vous pouvez choisir un exécuteur, qui détermine l'environnement dans lequel chaque job s'exécute. Par exemple, si vous voulez que votre job CI/CD exécute des commandes PowerShell, vous pouvez installer GitLab Runner sur un serveur Windows et ensuite enregistrer un runner qui utilise l'exécuteur shell. De même, si vous voulez que votre job CI/CD exécute des commandes dans un conteneur Docker personnalisé, vous pouvez installer GitLab Runner sur un serveur Linux et enregistrer un runner qui utilise l'exécuteur.

### MacOS

#### Avec Docker

Sur MacOS, Docker utilise des technologies de virtualisation pour créer un environnement Linux sur lequel Docker peut fonctionner, car Docker nécessite des fonctionnalités du noyau Linux qui ne sont pas disponibles sur macOS.

En conséquence, par défaut, le socket `/var/run/docker.sock` n'est pas monté. Pour le monter, il faut aller dans l'application, dans les paramètres avancés, et activer "Allow the default docker socket to be used". Un redémarrage sera requis.

Il sera sans doute nécessaire d'utiliser `docker login registry.gitlab.com` pour vous connecter auprès du registre gitlab.

#### Avec Podman

Sur MacOS, Podman également s'exécute dans un environnement Linux dédié.

Dans Podman desktop, en bas à gauche, cliquez sur "Docker compatibility".
Relancez la machine podman pour que les modifications prennent effet.
