# Guide de Création d'un Ticket pour un Problème DevOps

:::info

1. **Où créer le ticket** : Choisir le bon dépôt sur GitLab selon le projet.
2. **Publicité des échanges** : Les tickets sont publics, donc éviter d'inclure des informations sensibles.
3. **Eviter les doublons** : Rechercher si le problème n'a pas déjà été signalé.
4. **Format du ticket** : Inclure des détails tels que la description du problème, l'impact, la fréquence, l'environnement, les logs et un diagnostic préliminaire.
5. **Priorisation des tickets** : Évaluer correctement la priorité en fonction de l'urgence et de l'impact.
6. **Erreurs courantes** : Éviter les erreurs comme le manque de détails ou une mauvaise évaluation de la priorité.

:::

Ce guide est conçu pour aider les membres de l'équipe à soumettre des tickets de manière efficace et structurée, en fournissant toutes les informations nécessaires pour une résolution rapide des problèmes. En suivant ce format, vous aiderez l'équipe DevOps à comprendre et à traiter vos demandes plus efficacement, tout en assurant la sécurité et la confidentialité des informations sensibles.

## Où créer le ticket ?

Chaque projet a un dépôt d'infrastructure dédié, se trouvant dans le groupe [devops/projects](https://gitlab.com/incubateur-territoires/devops/projects/).
Si vous retrouvez votre projet ici, alors c'est le meilleur endroit pour créer votre demande.
Si votre projet ne concerne pas un projet dans cette liste, alors utilisez le projet global [d'infrastructure](https://gitlab.com/incubateur-territoires/devops/infrastructure).
Rendez-vous alors dans les issues, et créez-y un ticket.

## ⚠️ Publicité des échanges

Dans un souci de transparence et de simplicité, les tickets sont publics. Cela signifie que n'importe qui y a accès, sans restriction, incluant tout robot naviguant sur internet.

Il est donc important de ne pas inscrire de clefs, secrets, mot de passe, ou toute information revêtant un caractère secret.
Si cela venait à arriver, la règle est la même que pour un secret dans du code :
1. Je révoque le jeton qui a fuité
2. Je supprime du ticket

**La suppression du ticket sans révoquer l'accès et une faute grave**

Il est également important d'éviter d'y divulguer des vulnérabilités ou toute information permettant de mener des attaques informatiques.
Dans ce cas précis, il est possible, lors de la rédaction du ticket, de le marquer confidentiel en cochant la case "This issue is confidential and should only be visible to team members with at least Reporter access." Sous la description. Pour éviter toute fausse manipulation, cochez donc la case **avant** de commencer à renseigner la description.

## Doublons
N'hésitez pas à rechercher au préalable que le ticket que vous cherchez à créer n'existe pas déjà. Pour cela, utilisez la fonction de recherche [dans le groupe gitlab devops](https://gitlab.com/groups/incubateur-territoires/devops/-/issues) pour éviter d'être aveugle aux problématiques remontées sur d'autres projets.
N'hésitez pas, quand vous avez le même problème qu'un tiers, à ajouter un commentaire, en ajoutant toute information utile que vous avez. Si vous n'avez rien de plus à ajouter, indiquez seulement que vous êtes impacté.

## Format du Ticket

### Problème

* **Titre** : Indiquez ici soit ce que vous souhaitez résoudre, ou, si vous êtes certain·e de vous, ce que vous souhaitez être mis en œuvre.
* **Description** : Décrivez ici votre ticket. Plus il y a d'informations, plus il sera résolu rapidement. Plus l'information nous est facilement vérifiable et accessible, plus le problème sera résolu rapidement.
    - **Description détaillée** : Décrivez le problème de manière précise.
    - **Temporalité** : Indiquez _quand_ le problème s'est produit.
    - **Reproduction** : Étapes pour reproduire le problème, si applicable.
    - **Impact** : L'impact du problème sur les opérations / utilisateurs.
    - **Fréquence** : À quelle fréquence le problème se produit-il?
    - **Environnement** : Prod, Staging, Dev, etc.
        - **URL** : URL de l'environnement impacté
        - **Versions** : Version des systèmes concernés (ex : tag du logiciel que je déploie …).
        - **Configuration** : Liens vers la configuration utilisée
    - **Logs et Captures d'écran** : Montrer est souvent mieux qu'expliquer
        - **Logs** : Insérez tout log utile, _sous format texte_, veillez à ce qu'ils contiennent les timecodes, et retirez toute information sensible. Une alternative peut être un lien vers grafana. Auquel cas, précisez l'organisation sur lequel la requête pointe. Un lien vers un job gitlab en échec peut être également utile.
        - **Captures d'écran** : Si applicable, pour visualiser l'erreur.
    - **Diagnostic préliminaire** : Vous avez essayé quelque chose ? Indiquez-le.
        - **Actions déjà entreprises** : Liste des actions déjà entreprises pour tenter de résoudre le problème.
        - **Analyse initiale** : Toute analyse ou observation qui pourrait être utile.
    - **Informations supplémentaires** :
        - **Tickets liés** : Référence à d'autres tickets ou incidents liés.
        - **Documentation** : Liens vers la documentation pertinente.
* **Labels** : Vous pouvez ajouter des labels pour faciliter le tri
    - **environnement::\<prod|dev>** pour indiquer l'impact
    - **goal::_** pour indiquer l'objectif du ticket
    - **impact::_** pour indiquer quel service est impacté
    - **priority::\<critical|major|minor>** pour indiquer votre _auto évaluation_ de la criticité de votre problème. Il nous aide à trier uniquement, il ne donne pas de garantie quant à l'ordre de traitement.
    - **type::\<documentation|feature|bug>** pour indiquer quel type de demande vous adressez
    - **status::_** N'utilisez pas ce label, il nous est utile pour nous organiser et visualiser l'[issue board](https://gitlab.com/groups/incubateur-territoires/devops/-/boards/)
* **Assignation** : Il n'est pas nécessaire d'assigner le ticket
* **Epic, Weight, Due date, Iteration, Milestone** : laissez ces champs vides

### Cas particulier du déploiement d'un nouveau service

Dans le cas d'une requête en déploiement d'un nouveau service, il les informations essentielles diffèrent légèrement :

* **Titre** : Indiquez ici le service et l'environnement à déployer.
* **Description** : Décrivez ici votre ticket. Plus il y a d'informations, plus il sera résolu rapidement. Plus l'information nous est facilement vérifiable et accessible, plus le ticket sera traité rapidement.
    - **Description détaillée** : Décrivez l'objectif de manière précise.
    - **Ressources** : Toute information permettant de dimensionner les ressources (CPU, RAM, ...)
    - **Environnement** : Prod, Staging, Dev, etc.
    - **URL** : URL souhaitée de l'environnement à déployer
    - **Versions** : Version des logiciels à déployer.
    - **Configuration** : Liens vers la configuration à utiliser si nécessaire
    - **Documentation** : Liens vers la documentation du logiciel, au plus précis : Documentation Kubernetes, sinon, documentation docker, sinon documentation linux, sinon documentation générale.
    - **Informations supplémentaires** :
        - **Tickets liés** : Référence à d'autres tickets ou incidents liés.
        - **Documentation** : Liens vers la documentation pertinente.
* **Labels** : Vous pouvez ajouter des labels pour faciliter le tri
- **environnement::\<prod|dev>** pour indiquer l'environnement sur lequel déployer
- **priority::\<critical|major|minor>** pour indiquer votre _auto évaluation_ de la criticité de votre problème. Il nous aide à trier uniquement, il ne donne pas de garantie quant à l'ordre de traitement.
- **type::feature**
- **status::_** N'utilisez pas ce label, il nous est utile pour nous organiser et visualiser l'[issue board](https://gitlab.com/groups/incubateur-territoires/devops/-/boards/)
* **Assignation** : Il n'est pas nécessaire d'assigner le ticket
* **Due date** : Date à laquelle le service est attendu
* **Epic, Weight, Iteration, Milestone** : laissez ces champs vides

## Guidelines pour la Priorisation

**Critique (Critical)** : Sélectionnez cette priorité si le problème entraîne un arrêt complet ou une perte significative de fonctionnalités, affectant de nombreux utilisateurs et ne pouvant pas être contourné. Exemples : site web hors ligne, perte de données majeure.

**Majeur (Major)** : Utilisez cette priorité pour les problèmes qui affectent une fonctionnalité majeure sans alternatives de contournement immédiates, mais ne causent pas un arrêt complet. Exemple : fonctionnalité clé défaillante, performance gravement réduite.

**Mineur (Minor)** : Choisissez cette priorité pour les problèmes qui ont un impact limité sur l'opérationnalité, avec des contournements possibles. Exemples : problèmes d'interface utilisateur, petits bugs n'affectant pas les fonctionnalités principales.

**Évaluez l'Impact et l'Urgence** : considérez à la fois l'impact du problème sur les opérations et l'urgence de sa résolution pour déterminer la priorité appropriée.

**Évitez de surévaluer la Priorité** : ne marquez pas tout ticket comme critique ou majeur, car cela peut diluer l'attention sur les véritables urgences.

## Common Mistakes

- **Manque de Détails** : Soumettre un ticket sans informations suffisantes (comme des étapes de reproduction claires ou des logs) ralentit le processus de résolution.

- **Omission de l'Environnement ou de la Version** : Ne pas mentionner l'environnement (Prod, Dev, etc.) ou la version des systèmes concernés peut entraîner des confusions et des retards.

- **Duplication des Tickets** : Créer des tickets pour des problèmes déjà signalés crée du travail inutile. Toujours vérifier s'il existe déjà un ticket similaire.

- **Priorisation inappropriée** : Marquer des problèmes mineurs comme critiques détourne les ressources des véritables urgences.

- **Divulgation d'Informations sensibles** : Inclure des données sensibles comme des mots de passe ou des clés dans les tickets publics est une erreur de sécurité majeure.

- **Manque de Suivi** : Ne pas suivre l'évolution de votre ticket ou ne pas répondre aux demandes d'informations supplémentaires peut retarder la résolution.

## Relances

Une fois le ticket créé, si vous n'avez aucune nouvelle, n'hésitez pas à nous relancer dans le canal mattermost #incubateur-ops en précisant le lien vers le ticket.

**N'hésitez pas, également, à ajouter une mention des devOps dans le ticket pour assurer qu'ils reçoivent les notifications.**

## Exemples

Bien sûr, voici un exemple de bon et de mauvais ticket pour illustrer comment soumettre un problème via un ticket GitLab dans votre contexte DevOps.

### Bon Exemple de Ticket

**Titre :** Problème de connexion au serveur de base de données Prod

**Description :**  depuis ce matin, les tentatives de connexion à la base de données de production échouent, entraînant l'indisponibilité de l'application principale.
- **Temporalité :** le problème a commencé le 15 novembre 2023, à 09:00 CET.
- **Reproduction :** le problème survient à chaque tentative de connexion à la base de données depuis l'application.
- **Impact :** l'application principale est actuellement inutilisable pour tous les utilisateurs.
- **Fréquence :** Constante depuis le début du problème.
- **Environnement :** Prod. URL : [lien vers l'environnement impacté].
- **Versions :** Serveur DB version 12.3.1.
- **Configuration :** [Lien vers la configuration].
- **Logs et Captures d'écran :** [Lien vers les logs]. Aucune information sensible. Capture d'écran de l'erreur jointe.
- **Diagnostic préliminaire :** 
- **Actions déjà entreprises :** Redémarrage du serveur de base de données sans succès.
- **Analyse initiale :** pas de changements récents dans la configuration qui pourraient causer le problème.
- **Informations supplémentaires :**
- **Tickets liés :** aucun.
- **Documentation :** [Lien vers la documentation de l'application].

**Labels :** `environnement::prod`, `impact::other`, `type::bug`, `priority::critical`.

### Mauvais Exemple de Ticket

**Titre :** l'appli ne marche pas

**Description :**  l'application est cassée.
- **Temporalité :** depuis ce matin.
- **Reproduction :** j'ai essayé de l'ouvrir et ça ne fonctionne pas.
- **Impact :** c'est ennuyeux.
- **Fréquence :** je ne sais pas.
- **Environnement :** je ne sais pas.
- **Versions :** la dernière, je pense.
- **Configuration :** la configuration standard.
- **Logs et Captures d'écran :** pas pris.
- **Diagnostic préliminaire :** 
- **Actions déjà entreprises :** aucune.
- **Analyse initiale :** pas d'idée.
- **Informations supplémentaires :**
- **Tickets liés :** peut-être, je n'ai pas cherché.
- **Documentation :** je ne sais pas.

**Labels :** `type::bug`.