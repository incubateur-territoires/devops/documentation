# Glossaire

## Terraform

Terraform est un outil open source d'Infrastructure as Code (IaC) qui permet de définir et de fournir des infrastructures de datacenter en utilisant un langage de configuration déclaratif.

## Helm

Helm est un gestionnaire de packages pour Kubernetes. Il permet aux développeurs de packager, partager et déployer facilement des applications sur Kubernetes.

## Kubernetes

Kubernetes, aussi connu sous le nom de K8s, est un système open-source pour l'automatisation du déploiement, du dimensionnement et de la gestion des applications conteneurisées.

## Git

Git est un système de contrôle de version distribué gratuit et open source conçu pour gérer tout, de petits à de très grands projets, avec rapidité et efficacité.

## Docker

Docker est une plateforme open-source qui permet d'automatiser le déploiement, l'évolutivité et la gestion d'applications à l'intérieur de conteneurs logiciels.

## GitLab

GitLab est une plateforme web de gestion de dépôts Git pour le suivi de bugs, l'automatisation du déploiement continu et avec une syntaxe wiki pour la documentation.

## Infrastructure as Code (IaC)

Infrastructure as Code (IaC) est une pratique de DevOps qui utilise des fichiers de configuration pour gérer et provisionner les infrastructures informatiques.

## Déploiement Continu (CD)

Le Déploiement Continu (CD) est une pratique de DevOps où les modifications du code sont automatiquement déployées dans l'environnement de production.

## Intégration Continue (CI)

L'Intégration Continue (CI) est une pratique de DevOps où les développeurs fusionnent fréquemment leurs modifications de code dans un repository central. Après la fusion, les modifications sont validées par la création d'une build et par l'exécution de tests automatisés.

## Conteneur

Un conteneur est une unité logicielle standard qui regroupe le code et toutes ses dépendances afin que l'application puisse s'exécuter de manière rapide et fiable d'un environnement de calcul à un autre.
