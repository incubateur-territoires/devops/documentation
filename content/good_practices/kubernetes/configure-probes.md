---
sidebar_position: 1
sidebar_label: Configuration des probes
---

# Bonne configuration des probes

:::info

1. **Liveness Probe** : S'assure que l'application fonctionne correctement. Si elle échoue, le conteneur redémarre.
2. **Readiness Probe** : Contrôle si le conteneur peut recevoir du trafic. Si elle échoue, le trafic est redirigé vers d'autres pods.
3. **Startup Probe** : Utilisée pendant le démarrage du conteneur. Permet au conteneur de démarrer sans interruption par la Liveness Probe.

:::

Afin de configurer correctement les différentes probes disponibles sur Kubernetes, il est important de bien comprendre le fonctionnement de chacune d'entre elle.

## Liveness Probe

Cette probe s'exécute tout au long de la vie du conteneur. Lorsqu'elle échoue trop souvent, elle déclenche le redémarrage du conteneur.

Cette probe est utile pour débloquer une application qui se retrouverait dans un état l'empêchant de fonctionner (boucle infinie, traitement d'une requête trop long, perte de connexion à une DB sans réessayer...).

Il est conseillé de la configurer sur un endpoint dédié qui va faire quelques vérifications élémentaires et rapides (par exemple, tester la connexion avec la DB avec un `SELECT 1`).

## Readiness Probe

Cette probe s'exécute tout au long de la vie du conteneur. Lorsqu'elle échoue, cela indique au service de ne plus diriger de trafic vers ce conteneur.
Dans le cas d'un déploiement avec plusieurs réplicas, le trafic est alors habituellement redirigé vers au autre pod du déploiement.

Cette probe peut être utile pour aider à répartir la charge. Cependant, c'est un cas d'usage qui correspond à des services avec une grande volumétrie d'utilisateurs. Pour la plupart des applications web habituelles, cette probe peut être omise.

## Strartup Probe

Cette probe ne s'exécute que pendant le démarrage du conteneur. Une fois qu'elle a réussi une fois elle cesse de s'exécuter. Tant qu'elle n'a pas réussi, les autres probes ne démarrent pas.

Elle est utile pour laisser du temps au conteneur de démarrer sans être redémarré par la liveness probe.

Il est recommandé de toujours configurer la startup probe, avec un délai initial légèrement inférieur au temps de démarrage du conteneur, et avec une période très faible. Cela permet de détecter que le conteneur est prêt à recevoir du trafic le plus tôt possible.

## Exemple

Pour un conteneur qui met habituellement entre 13 et 17 secondes à démarrer et qu'on veut redémarrer s'il ne répond pas au bout de 15 secondes :
```yaml
startupProbe:
  # Au démarrage, on attend 10 secondes avant de démarrer la probe
  initialDelaySeconds: 10
  # On vérifie si le service a démarré toutes les secondes
  periodSeconds: 1
  # On permet au service de démarrer en 25 secondes maximum (10 + (15 * 1))
  # Au delà, le conteneur est redémarré
  failureThreshold: 15
  httpGet:
    path: /status
livenessProbe:
  # Une fois le conteneur démarré, on vérifie son état toutes les 5 secondes
  periodSeconds: 5
  # S'il échoue trois fois de suite, donc au bout de 15 secondes d'échec, il est redémarré
  failureThreshold: 3
  httpGet:
    path: /status
```
