# CRD cert-manager

:::info

1. **Certificate** : Représente un certificat TLS/SSL dans Kubernetes, stockant les informations de configuration et l'émetteur.
2. **Issuer et ClusterIssuer** : Entités émettrices de certificats, fonctionnant au niveau du namespace ou du cluster.
3. **CertificateRequest** : Demande de signature de certificat.
4. **Challenge** : Étape de validation de domaine dans le processus ACME.
5. **Order** : Demande de certificat auprès d'un serveur ACME.
6. **Policy** : Définit des règles pour l'émission de certificats.

:::

Cert-Manager est une extension pour Kubernetes qui automatise la gestion des certificats TLS/SSL. Il utilise des Custom Resource Definitions (CRD) pour étendre les fonctionnalités de Kubernetes. Voici quelques CRD clés utilisés par Cert-Manager et leur utilité :

1. **Certificate** :
    - Représente un certificat TLS/SSL dans le cluster.
    - Stocke les informations de configuration pour le certificat, telles que le nom commun, les noms d'hôte SAN (Subject Alternative Name), et l'Issuer qui doit signer le certificat.

2. **Issuer et ClusterIssuer** :
    - Représentent les entités qui peuvent émettre des certificats.
    - Un `Issuer` opère au niveau du namespace, tandis qu'un `ClusterIssuer` opère au niveau du cluster entier.
    - Ils stockent la configuration nécessaire pour émettre des certificats, par exemple, les informations d'authentification pour un serveur ACME comme Let's Encrypt.

3. **CertificateRequest** :
    - Représente une demande de signature de certificat (CSR - Certificate Signing Request).
    - Utilisé pour solliciter un `Issuer` ou un `ClusterIssuer` pour signer un certificat.

4. **Challenge** :
    - Représente un challenge ACME dans le processus de validation du domaine.
    - Utilisé par Cert-Manager pour prouver au serveur ACME que vous contrôlez le domaine pour lequel vous demandez un certificat.

5. **Order** :
    - Représente une demande de certificat auprès d'un serveur ACME.
    - Crée une ou plusieurs ressources `Challenge` pour compléter la validation du domaine.

6. **Policy** (dans les versions plus récentes de Cert-Manager) :
    - Permet de définir des règles pour l'émission des certificats, comme les contraintes sur les Issuers ou les paramètres du certificat.

Ces CRD permettent à Cert-Manager de gérer la création, le renouvellement, et la révocation des certificats TLS/SSL dans un cluster Kubernetes, en interagissant avec des autorités de certification externes comme Let's Encrypt, ou des autorités de certification internes. En utilisant ces CRD, les administrateurs et les développeurs peuvent définir la façon dont les certificats doivent être gérés et automatiser ce processus pour assurer une communication sécurisée entre les services et les utilisateurs.

```mermaid
graph TD;
    A[Issuer] -->|Peut émettre| B[Certificate];
    C[ClusterIssuer] -->|Peut émettre| B[Certificate];
    B -->|Génère| D[CertificateRequest];
    D -->|Demander| E[Order];
    E -->|Crée| F[Challenge];
    F -->|Valide| E;
    E -->|Si validé| B;
    G[Policy] -->|Définit les règles pour| B;
    
    classDef k8s fill:#326ce5,stroke:#ffffff,stroke-width:2px;
    class A,C,B,D,E,F,G k8s;
```
