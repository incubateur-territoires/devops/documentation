# Pages

Les GitLab Pages sont une fonctionnalité offerte par GitLab qui permet à l'utilisateur de créer des sites Web pour leurs projets, groupes ou utilisateurs directement à partir de leur dépôt GitLab. Ces pages sont généralement utilisées pour héberger des documents statiques, des blogs, des manuels et des documentations.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment pages, importez le fichier `pages.auto.yml`.

Ce fichier ajoute 1 étape :
* `pages` : Upload le dossier `dist/html` dans gitlab pages au tag
