# Deno

Deno est une plateforme d'exécution pour JavaScript et TypeScript qui est basée sur le moteur JavaScript V8 de Google et le langage de programmation Rust. Deno a été créé par Ryan Dahl, le créateur original de Node.js, et vise à résoudre certains des problèmes qu'il perçoit dans Node.js. Notamment, Deno supporte nativement TypeScript, offre une meilleure gestion des dépendances, et met l'accent sur la sécurité en exigeant des permissions explicites pour les opérations potentiellement dangereuses.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment deno, importez le fichier `deno.auto.yml`.

Ce fichier ajoute 5 étapes :
* `deno:cache` : Un travail de pré-stage qui met en cache les dépendances. Il n'actualise pas le fichier lock.json.
* `deno:format`, `deno:check` et `deno:lint` : Des travaux de test pour garantir la qualité du code.
* `deno:test` : Un travail qui nécessite la mise en cache pour l'exécution et qui produit une couverture dans le dossier cov/.

## Bonnes pratiques

### Maitrisez la version de deno utilisée

Modifiez le job caché `.vigigloo:deno:init` avec un tag d'image deno disponible sur le docker hub, afin d'être en maitrise de la version, et utiliser la même version dans la pipeline que dans votre image OCI, si vous en construisez une. Par défaut, `latest` est utilisé.

Pour cela, ajoutez le code suivant : 

```yml
.vigigloo:deno:init:
  image: denoland/deno:latest # Modifiez la valeur ici
```