# Docker

Docker est une plateforme de conteneurisation open-source qui permet de développer, d'expédier et d'exécuter des applications dans des environnements isolés appelés conteneurs. Un conteneur est une unité standardisée de logiciel qui regroupe le code, les dépendances et d'autres ressources, permettant à l'application de s'exécuter de manière cohérente dans divers environnements. Docker fournit une infrastructure pour gérer ces conteneurs, y compris leur création, leur déploiement et leur mise à l'échelle.

Kaniko, un projet de Google, est un outil pour construire des images Docker à partir d'un Dockerfile, similaire à la commande docker build. Cependant, Kaniko ne dépend pas du démon Docker et peut donc être utilisé dans des environnements où Docker ne peut pas être exécuté, comme les clusters Kubernetes standard ou certaines infrastructures de CI/CD. Kaniko prend un Dockerfile et le contexte associé, puis crée une image Docker qui peut être poussée vers un registre Docker.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment docker, importez le fichier `docker.auto.yml`.

Ce fichier ajoute 3 étapes :
* `docker:kaniko:build:production`: Compile le code Rust du projet.
* `docker:kaniko:build:development`: Exécute les tests du projet.
* `docker:kaniko:build:reviews`: Vérifie que le code suit les conventions de formatage de Rust.

## Comportement sous-jacent

Toute variable de CI commençant par `DOCKER_BUILD_ARG_` sera injecté au build comme argument.
`KANIKO_CACHE` permet d'activer le cache (vrai par défaut)
Toute variable de CI commençant par `CI_APPLICATION_TAG` permet d'ajouter un tag. Par défaut, `CI_APPLICATION_TAG` vaut le hash du commit, et `CI_APPLICATION_TAG_LATEST` vaut `latest`.