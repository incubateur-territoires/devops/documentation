# NPM

NPM (Node Package Manager) est une plateforme essentielle pour la gestion de paquets dans l'écosystème JavaScript/TypeScript. Elle permet aux développeurs d'installer, de partager et de gérer des paquets de code pour leurs projets. NPM fournit également des outils de gestion de version et de dépendance, facilitant la gestion de grands projets avec de nombreuses dépendances.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment npm, importez le fichier `npm.auto.yml`.

Ce fichier ajoute n étapes :
* `npm:install` : installe les dépendances du projet.
* `npm:lint` : vérifie la qualité du code.
* `npm:test` : exécute les tests du projet.
* `npm:generic-package` : prépare un package pour une éventuelle release.
* `npm:release` : publie une release

## Comportement sous-jacent

La variable `GENERIC_PACKAGE_NAME` est nécessaire pour les jobs de release, en son absence, ces étapes ne sont pas exécutées.


## Bonnes pratiques

### Maitrisez la version de node utilisée

Modifiez le job caché `.vigigloo:npm:install` avec un tag d'image node disponible sur le docker hub, afin d'être en maitrise de la version, et utiliser la même version dans la pipeline que dans votre image OCI, si vous en construisez une. Par défaut, `lts-alpine` est utilisé.

Pour cela, ajoutez le code suivant : 

```yml
.vigigloo:npm:install:
  image: node:lts-alpine # Modifiez la valeur ici
```
