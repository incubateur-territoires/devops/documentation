---
sidebar_position: 0
sidebar_label: Fragments de pipeline
---
# Introduction aux fragments de pipeline Gitlab

Un fragment de pipeline CI, aussi appelé "modèle de pipeline CI", est un segment de configuration de pipeline d'intégration continue (Continuous Integration, CI) qui peut être réutilisé dans plusieurs projets. Ces fragments sont généralement définis dans des fichiers séparés et peuvent être inclus dans la configuration principale de la pipeline CI d'un projet.

Le concept de fragments de pipeline est particulièrement utile pour structurer les pipelines CI de manière modulaire. Ils permettent de décomposer une configuration de pipeline complexe en plusieurs parties plus petites et plus gérables, qui peuvent être partagées et réutilisées dans différents projets.

Dans le contexte de GitLab, les fragments de pipeline sont définis en utilisant la syntaxe YAML, et peuvent être inclus dans la configuration principale de la pipeline en utilisant la clef include.

## Avantages

* **Réutilisabilité** : Les fragments de pipeline permettent de réutiliser des segments de configuration dans plusieurs projets, ce qui réduit la duplication de code et facilite la maintenance.
* **Modularité** : Ils permettent de décomposer une configuration de pipeline complexe en plusieurs parties plus petites et plus gérables.
* **Standardisation** : Ils peuvent aider à imposer des pratiques standard dans l'ensemble de l'organisation.
* **Déplacer la responsabilité** : Dans notre contexte, les fragments permettent de déplacer la charge de maintenance du projet vers l'équipe Ops.

## Inconvénients :

* **Complexité accrue** : L'utilisation de fragments de pipeline peut rendre la configuration de la pipeline plus complexe à comprendre, surtout si elle est répartie sur plusieurs fichiers.
* **Dépendances** : Si un fragment de pipeline est modifié, cela peut avoir des répercussions sur tous les projets qui l'incluent.

## Usage

Pour ajouter un fragment de pipeline, il suffit d'ajouter une partie "include" comme ci-dessous : 

```yaml
# .gitlab-ci.yml 
stages:
  - build # Ajoutez les stages et gérez l'ordre de ces derniers
include:
  project: vigigloo/gitlab-pipeline-fragments
  ref: v1 # Important pour éviter tout breaking change à l'avenir
  file:
    #  build
    - /docker.auto.yml # Exemple de fichier, relatif à la racine du dépot
```

Il est important d'ajouter la `ref`. En effet, dans le cas d'un changement majeur dont l'équipe n'est pas en mesure d'assurer la compatibilité ascendante, nous incrémenteront le numéro de référence afin d'assurer de ne pas casser.