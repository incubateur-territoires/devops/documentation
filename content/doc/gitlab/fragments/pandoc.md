# Pandoc

Pandoc est un convertisseur de documents universel en ligne de commande open-source. Il peut lire et écrire une multitude de formats de fichiers différents, permettant de convertir des documents d'un format à un autre. Par exemple, on peut convertir des fichiers markdown en HTML, Word, PDF, et beaucoup d'autres formats.

Ce fragment est utile surtout dans le cas où on a besoin de faire un livrable projet pour un prestataire chargé de MCO/MCS, et lui fournir dans ce livrable la documentation du projet.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment pandoc, importez un des fichiers suivants :

* `pandoc.all.auto.yml` : Génère automatique des fichiers PDF, ePub et Docx à partir de fichiers Markdown et export un artifact.
* `pandoc.pdf.auto.yml` : Génère automatique des fichiers PDF à partir de fichiers Markdown et export un artifact.
* `pandoc.epub.auto.yml` : Génère automatique des fichiers ePub à partir de fichiers Markdown et export un artifact.
* `pandoc.docx.auto.yml` : Génère automatique des fichiers Docx à partir de fichiers Markdown et export un artifact.

