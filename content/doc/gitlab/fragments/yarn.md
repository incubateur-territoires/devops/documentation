# Yarn

Yarn est un gestionnaire de paquets pour le code JavaScript. Il est compatible avec le système de gestion de paquets npm de Node.js, mais offre quelques fonctionnalités supplémentaires et des améliorations en termes de performances. Yarn permet d'installer des dépendances pour un projet JavaScript, de gérer les versions des paquets, et fournit également un système de verrouillage pour garantir la cohérence des installations de dépendances à travers les environnements.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment yarn, importez le fichier `yarn.auto.yml`.

Ce fichier ajoute 2 étapes :
* `yarn:install` : installe les dépendances du projet.
* `yarn:test` : exécute les tests du projet.

## Bonnes pratiques

### Maitrisez la version de node utilisée

Modifiez le job caché `.vigigloo:yarn:install` avec un tag d'image node disponible sur le docker hub, afin d'être en maitrise de la version, et utiliser la même version dans la pipeline que dans votre image OCI, si vous en construisez une. Par défaut, `latest` est utilisé.

Pour cela, ajoutez le code suivant : 

```yml
.vigigloo:yarn:install:
  image: node:latest # Modifiez la valeur ici
```
