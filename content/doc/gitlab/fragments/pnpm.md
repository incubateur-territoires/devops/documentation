# PNPM

PNPM est un gestionnaire de packages pour JavaScript qui est généralement utilisé comme alternative à npm ou Yarn. Il est réputé pour être plus rapide et plus efficace en termes de gestion de l'espace disque car il crée un stockage de packages unique sur votre ordinateur et y lie les packages nécessaires pour chaque projet. Cela évite la duplication des packages pour chaque projet, ce qui peut arriver avec npm ou Yarn.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment pnpm, importez le fichier `pnpm.auto.yml`.

Ce fichier ajoute 5 étapes :
* `pnpm:install` : installe les dépendances du projet.
* `pnpm:lint` : vérifie la qualité du code.
* `pnpm:test` : exécute les tests du projet.
* `pnpm:generic-package` : prépare un package pour une éventuelle release.
* `pnpm:release` : publie une release

## Comportement sous-jacent

La variable `GENERIC_PACKAGE_NAME` est nécessaire pour les jobs de release, en son absence, ces étapes ne sont pas exécutées.

## Bonnes pratiques

### Maitrisez la version de pnpm utilisée

Modifiez le job caché `.vigigloo:pnpm:install` avec un tag d'image node disponible sur le registre de conteneurs gitlab, afin d'être en maitrise de la version, et utiliser la même version dans la pipeline que dans votre image OCI, si vous en construisez une. Par défaut, `latest` est utilisé.

Pour cela, ajoutez le code suivant : 

```yml
.vigigloo:pnpm:install:
  image:
    name: registry.gitlab.com/vigigloo/tools/pnpm:latest # Modifiez la valeur ici
```

