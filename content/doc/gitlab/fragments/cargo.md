# Cargo

Cargo est l'outil de gestion des paquets de Rust, un langage de programmation moderne axé sur la sécurité et la performance. Il s'occupe de nombreuses tâches, telles que la construction de votre code, le téléchargement des dépendances nécessaires, et la compilation de ces dépendances. Cargo est également capable de tester votre code et de construire votre documentation.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment cargo, importez le fichier `cargo.auto.yml`.

Ce fichier ajoute 4 étapes :
* `cargo:build`: Compile le code Rust du projet.
* `cargo:test`: Exécute les tests du projet.
* `cargo:fmt`: Vérifie que le code suit les conventions de formatage de Rust.
* `cargo:clippy`: Exécute Clippy, un outil d'analyse de code Rust.

## Comportement sous-jacent

La pipeline utilise de manière sous-jacente le projet SCCache afin de tenter d'améliorer les performances de compilation.

## Bonnes pratiques

### Maitrisez la version de rust utilisée

Modifiez la variable `RUST_VERSION` avec un tag d'image Rust disponible sur le docker hub, afin d'être en maitrise de la version, et utiliser la même version dans la pipeline que dans votre image OCI, si vous en construisez une. Par défaut, `latest` est utilisé.