# Helm

Helm est un gestionnaire de paquets pour Kubernetes. Il permet aux développeurs et aux opérateurs de gérer facilement les déploiements Kubernetes. Helm utilise un format d'emballage appelé "chart" qui prépackage les ressources de l'application, vous permettant de déployer et de gérer des applications complexes grâce à des commandes simples. En gros, Helm est à Kubernetes ce que apt est à Ubuntu ou yum à CentOS.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment helm, importez le ou les fichiers correspondant au workflow que vous souhaitez :
* `helm.development.auto.yml` : Déploie sur un environnement de development automatiquement au merge sur la branche par défaut.
* `helm.development.manual.yml` : Déploie sur un environnement de development au clic sur l'étape de CI en déploiement manuel.
* `helm.production.auto.yml` : Déploie sur un environnement de production automatiquement au tag.
* `helm.reviews.auto.yml` : Déploie sur un environnement de review automatiquement sur un nouvel environnement pour chaque branche.
* `helm.reviews.manual.yml` : Déploie sur un environnement de review au clic sur l'étape de CI en déploiement manuel.

## Comportement sous-jacent

* Toute variable de CI commençant par `HELM_ENV_VAR_` sera injecté au runtime comme variable d'environnement.
* `HELM_UPGRADE_EXTRA_ARGS` permet d'ajouter des paramètres `helm` de manière arbitraire.
Toute variable de CI commençant par `HELM_INGRESS_` permet d'ajouter une configuration sous la clef `ingress`.
* `HELM_TIMEOUT` (vide par défaut) sert à étendre le timeout du déploiement.
* `HELM_CHART_VERSION` permet de spécifier la version du chart à utiliser.
* `HELM_NAME` (requis) permet de nommer le déploiement.
* `HELM_CHART` (requis) permet de donner la source du chart à déployer.
* `FORCE_HELM_IMAGE_CREDENTIALS` peut permettre de forcer sur un dépot public l'utilisation de jeton de déploiement.