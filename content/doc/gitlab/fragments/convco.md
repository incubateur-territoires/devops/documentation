# Convco

Convco est une interface en ligne de commande (CLI) qui aide à contrôler les "conventional commits". Les Conventional Commits sont une convention pour les messages de commit qui est simple, mais qui offre de nombreux avantages lorsqu'ils sont utilisés correctement. Ils facilitent la génération automatique de notes de version, rendent plus facile la navigation dans le journal de git, et aident à comprendre l'intention derrière un commit donné.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment convco, importez le fichier `convco.auto.yml`.

Ce fichier ajoute 1 étape :
* `convco:check`: Vérifie l'historique des messages git
