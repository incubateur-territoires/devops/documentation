# Releaser

Le projet MetaReleaser est un outil qui vise à être utilisé pour automatiser la génération d'un package.
Il lit un fichier toml suivant des instructions simples.

Ce fragment est utile surtout dans le cas où on a besoin de faire un livrable projet pour un prestataire chargé de MCO/MCS, et lui fournir dans ce livrable la documentation du projet.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment releaser, importez le fichier `releaser.auto.yml`.

Ce fichier ajoute 2 étapes :
* `releaser:build` : prépare un package pour une éventuelle release.
* `releaser:release` : publie une release

## Comportement sous-jacent

La variable `GENERIC_PACKAGE_NAME` est nécessaire pour les jobs de release, en son absence, ces étapes ne sont pas exécutées.

Il cherche un fichier `metareleaser.toml` pour en lire la config (ou préciser le chemin via `$CONFIG_PATH`)
