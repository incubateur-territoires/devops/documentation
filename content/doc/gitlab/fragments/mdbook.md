# mdBook 

mdBook est un outil de création de livre à partir de fichiers markdown. Il est largement utilisé par la communauté Rust pour la documentation, mais il peut être utilisé pour tout type de livre. mdBook produit une sortie HTML qui est facile à naviguer et à lire.

## Import automatique

Pour importer le comportement automatique et par défaut du fragment mdbook, importez le fichier `mdbook.auto.yml`.

Ce fichier ajoute 1 étape :
* `build:docs:mdbook` : Exécute mdbook
