# Fragments "proxy"

Gitlab fournie un certain nombre de fragments de CI.
Dans le dossier proxy, on réimporte ces fichiers, en modifiant parfois des variables d'environnement permettant de modifier leurs comportements par défaut afin d'être soit conforme avec notre usage par défaut, soit permettre d'étendre une période de flexibilité lors d'une mise à jour de gitlab cassant le comportement d'un fichier de CI. (Chose arrivée au moins 1 fois par le passé).