# Commandes essentielles de Kubernetes CLI

## Gestion des ressources

**Afficher toutes les ressources :**
```bash
kubectl get all
```

**Afficher des ressources spécifiques (pods, services, etc.) :**
```bash
kubectl get <type de ressource>
```

**Afficher les détails d'une ressource spécifique :**
```bash
kubectl describe <type de ressource> <nom de la ressource>
```
**Supprimer une ressource :**
```bash
kubectl delete <type de ressource> <nom de la ressource>
```
## Gestion des pods

**Afficher tous les pods :**
```bash
kubectl get pods
```

**Afficher les détails d'un pod spécifique :**
```bash
kubectl describe pod <nom du pod>
```

**Exécuter une commande dans un pod spécifique (utile pour le débogage) :**
```bash
kubectl exec -it <nom du pod> -- <commande>
```

**Afficher les logs d'un pod :**
```bash
kubectl logs <nom du pod>
```

## Gestion des services

**Afficher tous les services :**
```bash
kubectl get services
```
**Afficher les détails d'un service spécifique :**
```bash
kubectl describe service <nom du service>
```

Cette documentation n'est qu'une introduction aux commandes kubectl. Pour une liste complète des commandes, consultez [la documentation officielle de kubectl](https://kubernetes.io/fr/docs/reference/kubectl/overview/).
