# Commandes essentielles utiles pour grafana

## Loki

Loki est utilisé pour la collecte de logs. Voici des commandes utiles

### Recherche Basique

- **Tous les logs d'une application** :
  ```plaintext
  {app="my-app"}
  ```

- **Logs avec un contenu spécifique** :
  ```plaintext
  {app="my-app"} |= "error"
  ```

- **Exclure des logs avec un contenu spécifique** :
  ```plaintext
  {app="my-app"} != "info"
  ```

- **Recherche avec plusieurs filtres** :
  ```plaintext
  {app="my-app",environment="production"} |= "error" != "timeout"
  ```

- **Recherche avec expressions régulières** :
  ```plaintext
  {app="my-app"} |~ "error|warning"
  ```

### Manipulation de Strings

- **Extraire une partie du log** :
  ```plaintext
  {app="my-app"} |~ "(.*)_error" | line_format "{{.match}}"
  ```

- **Remplacer du texte dans les logs** :
  ```plaintext
  {app="my-app"} | replace ("old_string", "new_string")
  ```

### Fonctions d'Aggrégation

- **Compter le nombre de logs** :
  ```plaintext
  {app="my-app"} | count_over_time(1h)
  ```

- **Calculer la moyenne des valeurs** :
  ```plaintext
  {app="my-app"} | json | avg_over_time(duration_seconds[1h])
  ```

- **Calculer le 95th percentile** :
  ```plaintext
  {app="my-app"} | json | quantile_over_time(0.95, duration_seconds[1h])
  ```

### Fonctions de Formatage

- **Afficher les labels** :
  ```plaintext
  {app="my-app"} | labels
  ```

- **Afficher des statistiques de comptage** :
  ```plaintext
  {app="my-app"} | stats count by (level)
  ```

### Divers

- **Réduire le bruit des logs** :
  ```plaintext
  {app="my-app"} |= "error" | uniq
  ```

- **Trier les logs** :
  ```plaintext
  {app="my-app"} | sort desc
  ```

## Prometheus

### Recherche Basique

- **Requêter une métrique spécifique** :
  ```plaintext
  http_requests_total
  ```

- **Requêter avec des labels spécifiques** :
  ```plaintext
  http_requests_total{method="GET", status="200"}
  ```

### Fonctions d'Aggrégation

- **Somme** :
  ```plaintext
  sum(http_requests_total)
  ```

- **Moyenne** :
  ```plaintext
  avg(http_requests_total)
  ```

- **Minimum et Maximum** :
  ```plaintext
  min(http_requests_total)
  max(http_requests_total)
  ```

- **Compter le nombre d'instances** :
  ```plaintext
  count(http_requests_total)
  ```

### Opérateurs

- **Addition, Soustraction, Multiplication, Division** :
  ```plaintext
  http_requests_total + 100
  http_requests_total - 100
  http_requests_total * 2
  http_requests_total / 2
  ```

### Fonctions de Taux

- **Taux d'erreur sur 5 minutes** :
  ```plaintext
  rate(http_requests_total{status=~"5.."}[5m])
  ```

- **Taux de requêtes sur 1 minute** :
  ```plaintext
  rate(http_requests_total[1m])
  ```

### Fonctions Temporelles

- **Métrique la plus récente dans les dernières 5 minutes** :
  ```plaintext
  max_over_time(http_requests_total[5m])
  ```

- **Métrique la plus ancienne dans les dernières 5 minutes** :
  ```plaintext
  min_over_time(http_requests_total[5m])
  ```

### Histogrammes et Percentiles

- **Calculer le 95th percentile** :
  ```plaintext
  histogram_quantile(0.95, sum(rate(http_request_duration_seconds_bucket[5m])) by (le))
  ```

### Groupement et Étiquetage

- **Somme par méthode** :
  ```plaintext
  sum(http_requests_total) by (method)
  ```

- **Renommer les labels** :
  ```plaintext
  label_replace(http_requests_total, "new_label", "$1", "old_label", "(.*)")
  ```
