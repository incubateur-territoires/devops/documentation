# Procédure de Recherche de Logs sur Grafana via Explore et Loki

Cette documentation présente la procédure pour rechercher des logs sur Grafana en utilisant la fonction Explore et la datasource Loki. Loki est un système d'agrégation de logs hautement performant et Grafana est un outil de visualisation et d'analyse open-source.

## Accès à Grafana

1. **URL de Grafana** : Accédez à l'interface de Grafana via l'URL fournie.
2. **Authentification** : Authentifiez-vous avec vos identifiants.

## Sélectionner Explore

1. **Menu Hamburger** : Cliquez sur l'icône du menu hamburger (☰) en haut à gauche.
2. **Explore** : Sélectionnez "Explore" dans le menu.

## Sélectionner la Datasource Loki

1. Dans le menu déroulant en haut, sélectionnez **Loki** comme votre datasource.

## Recherche de Logs

### Syntaxe de Base

- **Label Filtering** : `{label="value"}`
- **Log Content Filtering** : `{label="value"} |= "filter"`

### Exemples de Commandes

=> [Cheatsheet](./cheatsheet.md)

## Analyse des Logs

- **Panneau de Logs** : Les résultats seront affichés dans le panneau de logs.
- **Highlights** : Mettez en surbrillance des mots-clés spécifiques pour faciliter l'analyse.
- **Graphiques** : Utilisez les graphiques pour visualiser la fréquence des logs.
