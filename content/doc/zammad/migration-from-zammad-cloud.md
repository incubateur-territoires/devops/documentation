# Migration de Zammad Cloud vers Kubernetes

## Prérequis
- kubectl installé et configuré pour communiquer avec votre cluster.

## Exportation de vos données de Zammad Cloud
- Confirmez la version de Zammad présente sur la version Cloud
- Demandez un export auprès du support
- Le support fourni une archive à télécharger, cette archive contient le backup du stockage ainsi que le backup du postgresql
- Récupérez et téléversez les deux backups sur S3

## Préparation de l'environnement Kubernetes
- Déployez le chart helm officiel avec une version >= à celle du backup

## Restoration du backup
- Restorez l'archive de base de données
- Exécutez un pod sur le même noeud que le pod zammad, afin d'y restorer les fichiers :
  - Récupérez le nom du noeud sur lequel se trouve Zammad via `kubectl get pod -o wide`
  - Récupérez le nom du pvc qui contient les données
  - Créez un pod de ce type : 
    ```yaml
      apiVersion: v1
      kind: Pod
      metadata:
        name: ubuntu
        labels:
          env: test
      spec:
        containers:
          - name: ubuntu
            image: ubuntu
            imagePullPolicy: IfNotPresent
            command:
              - "sleep"
              - "604800"
            volumeMounts:
              - mountPath: /opt/zammad/storage
                name: zammad
        nodeSelector:
          kubernetes.io/hostname: <nom du noeud, exemple scw-incubateur-production-small-xxxxxxxxx>
        volumes:
          - name: zammad
            persistentVolumeClaim:
              claimName: <nom du pvc, exemple zammad-var-zammad-0>
    ```
  - Utilisez `exec` pour interagir avec le container ainsi créé : `kubectl exec ubuntu -it /bin/bash`
    - Installez avec apt les quelques outils dont vous aurez besoin : `apt update && apt install -y vim rclone ca-certificates`
    - Configurez rclone pour récupérer l'archive, en utilisant `rclone config` ou en créant un fichier `/root/.config/rclone/rclone.conf` avec un contenu comme suit :
      ```
        [anct-prod]
        type = s3
        provider = Scaleway
        access_key_id = <access_key>
        secret_access_key = <secret_key>
        region = fr-par
        endpoint = s3.fr-par.scw.cloud
        acl = private
      ```
    - Récupérez l'archive via `rclone copy anct-prod:<nom du bucket>/<chemin de l'archive> ./`
    - Décompressez l'archive via `tar -xvf <chemin de l'archive>`
    - Dans nos tests, l'archive décompressait depuis `/` vers `opt/zammad/storage` (donc vers `/opt/zammad/storage`, qui correspondait bien au path du PVC), vérifiez qu'il en est bien de même pour vous.
    - Corrigez les droits du filesystem via `chown -R 1000:1000 <chemin des fichiers, ici /opt/zammad/storage>`
    - Vous pouvez maintenant quitter avec `exit` et supprimer votre container via `kubectl delete pod ubuntu`
  - Connectez-vous dans le container railserver de zammad via `kubectl exec zammad-0 -c zammad-railsserver -it /bin/bash`
    - Exécutez la commande `rails r "Setting.set('es_url', 'http://<chemin vers le service elasticsearch>:9200')"` pour connecter Zammad à ElasticSearch
    - Exécutez la commande `rake zammad:searchindex:rebuild` pour reconstruire l'index ElasticSearch (peut être très long)
- Dans le backoffice de Zammad, vérifiez le nom de domaine (tout changement nécessitera de relancer Zammad, pour cela, le plus simple est de kill le pod)
- Dans le backoffice de Zammad, vérifiez la connexion SMTP

## Notes utiles

Vous pouvez vous connecter au pod zammad railserver comme au dessus et exécuter la commande `rails c` pour ouvrir un CLI.

Si besoin de modifier un mot de passe, alors que vous n'avez pas de SMTP configuré (et donc impossible d'utiliser le reset de mot de passe), utilisez le CLI et la commande `User.find_by(email: 'you@example.com').update!(password: 'your_new_password')`.

Pour vous donner les droits admin, procédez ainsi : 
```
>> u = User.find_by(email: 'you@example.com')
>> u.roles = Role.where(name: ['Agent', 'Admin'])
>> u.save!
```

## Documentation & sources
- [🇬🇧 [CLI] Console](https://docs.zammad.org/en/latest/admin/console.html)
- [🇬🇧 [CLI] Working on user information](https://docs.zammad.org/en/latest/admin/console/working-on-users.html)
- [🇬🇧 ElasticSearch configuration](https://docs.zammad.org/en/latest/install/elasticsearch.html)
