import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import HomepageFeatures from '@site/src/components/HomepageFeatures';
import Layout from '@theme/Layout';
import clsx from 'clsx';
import React from 'react';

import styles from './index.module.css';

function HomepageHeader() {
  const { siteConfig } = useDocusaurusContext();

  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className="container">
        <h1 className="hero__title">{siteConfig.title}</h1>
        <p className="hero__subtitle">{siteConfig.tagline}</p>
        <div className={styles.buttons}>
          <Link className="button button--danger button--lg" to="/content/doc">
            Consulter la documentation
          </Link>
        </div>
      </div>
    </header>
    );
}

export default function Home(): JSX.Element {
  return (
    <Layout
      title={`Accueil`}
      description="Documentation de l'équipe DevOps de l'incubateur des territoires à destination des collaborateurs de l'incubateur."
      >
      <HomepageHeader />
      <main>
        <HomepageFeatures />
      </main>
    </Layout>
    );
}