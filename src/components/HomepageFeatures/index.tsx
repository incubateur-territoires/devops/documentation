import { useColorMode } from '@docusaurus/theme-common';
import clsx from 'clsx';
import React from 'react';

import styles from './styles.module.css';

type FeatureItem = {
  title: string;
  SvgLight: React.ComponentType<React.ComponentProps<'svg'>>;
  SvgDark: React.ComponentType<React.ComponentProps<'svg'>>;
  description: JSX.Element;
};

const FeatureList: FeatureItem[] = [
];

function Feature({ title, SvgLight, SvgDark, description }: FeatureItem) {
  const { isDarkTheme } = useColorMode();

  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        {isDarkTheme ? <SvgDark className={styles.featureSvg} role="img" /> : <SvgLight className={styles.featureSvg} role="img" />}
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
    );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
            ))}
        </div>
      </div>
    </section>
    );
}