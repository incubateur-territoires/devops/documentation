# Comment contribuer à ce projet

## Pré-requis

Avant de commencer, assurez-vous que vous avez une connaissance de base de Git. Si vous êtes nouveau à Git, nous vous recommandons de consulter ce [guide de Git](https://git-scm.com/book/fr/v2).

## GitLab Flow

Nous utilisons le GitLab Flow pour la gestion de notre code source. Voici une description de base de notre processus :

1. **Choisissez une issue** : Choisissez une issue sur laquelle travailler dans notre liste d'issues. Si vous souhaitez travailler sur quelque chose qui n'est pas encore une issue, documentez directement dans la merge request. Il peut être cependant important, pour éviter du travail inutile, de créer une issue au préalable ou discuter avec l'équipe ops en amont.

2. **Créez une nouvelle branche** : Créez une nouvelle branche à partir de la branche `master` pour chaque nouvelle fonctionnalité ou correction. Nommez votre branche en fonction de l'issue que vous résolvez (par exemple, `feature/<nom-de-la-fonctionnalité>` ou `fix/<description-du-bug>`).

3. **Travaillez sur votre branche** : Faites vos modifications sur cette branche. Assurez-vous que votre code respecte nos normes de codage et que tous les tests passent.

4. **Commit et Push** : Committez régulièrement vos modifications et poussez-les vers le repository. Assurez-vous d'écrire des messages de commit clairs et descriptifs.

5. **Ouvrez une Merge Request (MR)** : Une fois que vous avez terminé votre travail, ouvrez une MR vers la branche `develop`. Décrivez ce que fait votre MR et liez l'issue qu'elle résout.

6. **Review** : Attendez que quelqu'un de l'équipe examine votre MR. Ils peuvent avoir des commentaires ou des suggestions. Apportez les modifications demandées, si nécessaire.

7. **Merge** : Une fois votre MR approuvée, elle sera fusionnée avec la branche `develop`.

## Code de conduite

Nous nous engageons à fournir un environnement accueillant et sûr pour tous les contributeurs. Veuillez lire et respecter notre [Code de Conduite](./CODE_OF_CONDUCT.md).

## Questions

Si vous avez des questions ou besoin d'aide, n'hésitez pas à nous contacter via la création d'une Issue.

Merci pour votre intérêt à contribuer à notre projet !
