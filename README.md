# Documentation de l'infrastructure DevOps IaC

Bienvenue dans le repository de la documentation de notre infrastructure DevOps Infrastructure as Code (IaC). 
Ce repository est maintenu par notre équipe au sein de l'incubateur des territoires.

## Description

Ce repository contient la documentation de notre infrastructure DevOps IaC, qui explique comment nous gérons et automatisons nos infrastructures de serveur. Le but de cette documentation est de fournir des informations détaillées et à jour sur notre approche et nos pratiques.

Bien que nos documentations intra-code soient normées en anglais, la documentation vise un public francophone, et son contenu devra être en français, en conformité avec la [loi n° 94-665 du 4 août 1994 relative à l'emploi de la langue française](https://www.legifrance.gouv.fr/loda/id/LEGITEXT000005616341).

## Contenu

- **Recettes de cuisine** : Ces recettes sont des guides pratiques pour répondre à une problématique donnée
- **Documentation** : Explique en détail les points de fonctionnement, peut inclure des documentations des outils tiers utilisés, complétés par des liens vers la documentation officielle.
- **Bonnes pratiques** : Nos recommandations et meilleures pratiques pour travailler avec l'infrastructure.
- **FAQs** : Réponses aux questions les plus fréquemment posées.
- **Glossaire** : Explications des termes techniques utilisés dans ce repository.

## Contribution

Si vous souhaitez contribuer à ce projet, veuillez lire notre [guide de contribution](./CONTRIBUTING.md). Toutes les contributions sont les bienvenues, qu'il s'agisse de corrections, de mises à jour de la documentation existante ou de nouvelles suggestions.

## Exécuter ce projet

Ce projet est construit avec Nuxt. Pour une documentation complète sur la façon de construire le projet, référez vous à la [documentation de Nuxt](https://nuxt.com/).

## License

Ce repository est sous la licence Affero General Public License (AGPL). Vous pouvez consulir le fichier [LICENSE](./LICENSE) pour plus d'informations.

## Contact

Si vous avez des questions, des préoccupations ou des suggestions, n'hésitez pas à utiliser les fonctionnalités d'Issues).

Merci pour votre intérêt pour notre projet!
