# Code de Conduite

## Nos normes

Exemples de comportement qui contribuent à créer un environnement positif :

- Accepter gracieusement les critiques constructives.
- Faire preuve d'empathie envers les autres.

Exemples de comportement inacceptable :

- Les commentaires insultants/dégradants, et les attaques personnelles ou politiques.
- Le harcèlement public ou privé.
- Publier les informations privées d'autrui, telles que des informations physiques ou électroniques, sans permission explicite.
- Autres comportements qui pourraient raisonnablement être considérés comme inappropriés dans un cadre professionnel.

## Nos responsabilités

Les mainteneurs du projet sont responsables de clarifier et de faire respecter les normes de comportement acceptable et prendront des actions correctives appropriées et équitables en réponse à tout comportement inacceptable.

Les mainteneurs du projet ont le droit et la responsabilité de supprimer, modifier ou rejeter les commentaires, les commits, le code, les modifications du wiki, les issues et autres contributions qui ne respectent pas ce Code de Conduite, et de bannir temporairement ou de façon permanente tout contributeur pour des comportements qu'ils jugent inappropriés, menaçants, offensants ou nuisibles.

## Application

Les cas de comportement abusif, de harcèlement ou autrement inacceptable peuvent être signalés en contactant l'équipe du projet. Toutes les plaintes seront examinées.

Tous les mainteneurs du projet sont tenus de respecter la confidentialité et la sécurité des personnes qui signalent un incident.

À noter que le projet évolue au sein d'un incubateur de services publics, composé d'agents publics, soumis à l'obligation de l'[article 40 du code de procédure pénal](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006574933).

## Attribution

Ce Code de Conduite est adapté du [Contributor Covenant](https://www.contributor-covenant.org), version 2.0, disponible à [https://www.contributor-covenant.org/version/2/0/code_of_conduct.html](https://www.contributor-covenant.org/version/2/0/code_of_conduct.html).
